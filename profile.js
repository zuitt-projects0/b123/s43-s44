let token = localStorage.getItem('token')
let name = document.querySelector('#profile-name')
let email = document.querySelector('#profile-email')
let mobile = document.querySelector('#profile-mobile')
let mainJum = document.querySelector('#div1')

console.log(token)

fetch('http://localhost:4000/users/getUserDetails', {
	headers:{
		'Authorization': `Bearer ${token}`
	}
})
.then(res => res.json())
.then(data=>{
/*	let profileCards = ""
	console.log(data)
	profileCards += `
		<div  class="jumbotron">
			<h3 class="text-center" id="profile-name">${data.firstName} ${data.lastName}</h3>
			<p class="text-center" id="profile-email">${data.email}</p>
			<p class="text-center" id="profile-mobile">${data.mobileNo}</p>
		</div>
	`
	mainJum.innerHTML = profileCards*/
	name.innerHTML = `${data.firstName} ${data.lastName}`
	email.innerHTML = `${data.email}`
	mobile.innerHTML = `${data.mobileNo}`
})
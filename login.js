let emailInput = document.querySelector("#email-input")
let passwordInput = document.querySelector("#password-input")

document.querySelector('#form-register').addEventListener('submit',(e)=>{
	e.preventDefault()

	console.log(emailInput.value)
	console.log(passwordInput.value)

	fetch('http://localhost:4000/users/login',{
		method: 'POST',
		headers:{
			'Content-Type': 'application/json'
		},
		body: JSON.stringify({
			email:emailInput.value,
			password:passwordInput.value,
		})
	})
	.then(res=>res.json())
	.then(data=>{
		console.log(data)
		localStorage.setItem('token',data.accessToken)
	})
})
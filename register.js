let firstNameInput = document.querySelector("#first-name-input")
let lastNameInput = document.querySelector("#last-name-input")
let mobileNoInput = document.querySelector("#mobile-num-input")
let emailInput = document.querySelector("#email-input")
let passwordInput = document.querySelector("#password-input")
let isAdmin = document.querySelector("#isAdmin-input")

document.querySelector('#form-register').addEventListener('submit',(e)=>{
	e.preventDefault()

	console.log(firstNameInput.value)
	console.log(lastNameInput.value)
	console.log(mobileNoInput.value)
	console.log(emailInput.value)
	console.log(passwordInput.value)
	//console.log(isAdmin.value)

	fetch('http://localhost:4000/users/',{
		method: 'POST',
		headers:{
			'Content-Type': 'application/json'
		},
		body: JSON.stringify({
			firstName:firstNameInput.value,
			lastName:lastNameInput.value,
			email:emailInput.value,
			mobileNo:mobileNoInput.value,
			password:passwordInput.value,
			isAdmin:isAdmin.value
		})
	})
	.then(res=>res.json())
	.then(data=>{
		console.log(data)
	})
})
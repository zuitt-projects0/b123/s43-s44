let cName = document.querySelector("#course-input")
let cDesc = document.querySelector("#desc-input")
let cPrice = document.querySelector("#price-input")
let token = localStorage.getItem('token')
let submitz = document.querySelector("#submit-btn")

document.querySelector('#form-register').addEventListener('submit',(e)=>{
	e.preventDefault()
	fetch('http://localhost:4000/courses/',{
		method: 'POST',
		headers:{
			'Content-Type': 'application/json',
			'Authorization': `Bearer ${token}`
		},
		body: JSON.stringify({
			name:cName.value,
			description:cDesc.value,
			price:cPrice.value,
		})
	})
	.then(res=>res.json())
	.then(data=>{
		console.log(data)
	})
	cName.value =""
	cDesc.value =""
	cPrice.value =""
})
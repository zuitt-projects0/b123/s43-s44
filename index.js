let coursesDiv = document.querySelector("#courses-div")
console.log(coursesDiv)
/*console.log(coursesDiv.innerHTML)

let mainDiv = document.querySelector("#main-div")
console.log(mainDiv.innerHTML)

mainDiv.innerHTML = "<h1>Batch 123!</h1>"
mainDiv.innerHTML += "<p>Full Stack Developers</p>"
mainDiv.innerHTML = mainDiv.innerHTML + "<p>My Name</p>"*/

fetch('http://localhost:4000/courses/getActiveCourses')
.then(res => res.json())
.then(data => {
	//console.log(data)
	let courseCards = ""
	data.forEach((course)=>{
		console.log(course)
		courseCards += `
			<div class = "card">
				<h4>${course.name}</h4>
				<p>${course.description}</p>
				<span>${course.price}</span>
			</div>
		`
	})
	console.log(courseCards)
	coursesDiv.innerHTML = courseCards
})